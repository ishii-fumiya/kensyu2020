<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>二次元配列</title>
  </head>

  <body>
    <table border="1">
      <?php
        $team_a = array("今井","浜口","平","石田","東");
        $team_b = array("伊藤","高橋","峯井","井戸","益子");
        $team_c = array("佐野","乙","細川","梶原","神");
        $team_d = array("宮崎","芝","大和","石川","蔵元");
        $team_e = array("三島","三上","国","砂","武藤");

        $team_all = array($team_a, $team_b, $team_c, $team_d, $team_e);

        echo "<pre>";
        var_dump($team_all);
        echo "</pre>";

        echo "<table border='1'>";
          foreach($team_all as $team){
            echo "<tr>";
              foreach($team as $value){
                echo "<td>" . $value . "</td>";
              }
            echo "</tr>";
          }
        echo "</table>";
      ?>

  </body>
</html>
