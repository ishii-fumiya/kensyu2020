<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>二次元配列</title>
  </head>

  <body>
      <?php
        $bread01 = array(
          "id" => "100",
          "name" => "アンパン",
          "position" =>"主役",
          "from" => "工場",
          "year" => "2000",
        );
        $bread02 = array(
          "id" => "200",
          "name" => "カレーパン",
          "position" =>"脇役",
          "from" => "工場",
          "year" => "2001",
        );
        $bread03 = array(
          "id" => "300",
          "name" => "ショクパン",
          "position" =>"脇役",
          "from" => "工場",
          "year" => "2002",
        );

        $bread = array($bread01, $bread02, $bread03);

        echo "<pre>";
        var_dump($bread);
        echo "</pre>";

        echo "<hr/>";

      //   foreach($bread as $each){
      //     echo"出荷番号: " . $each['id'] . ", "
      //       . "名前: " . $each['name'] . ", "
      //       . "ポジション: " . $each['position'] . ", "
      //       . "出身地: " . $each['from'] . ", "
      //       . "期限: " . $each['year'] . "<br/>";
      //       echo "</table>";
      //   }
      //
      //   echo "<hr/>";

      ?>
    <table border='1'>
      <tr>
      <th>出荷番号</th>
      <th>名前</th>
      <th>ポジション</th>
      <th>出身地</th>
      <th>期限</th>
    </tr>
<?php
  foreach($bread as $each){
    echo
    "<tr>" .
      "<td>" . $each['id'] . "</td>".
      "<td>" . $each['name'] . "</td>".
      "<td>" . $each['position'] . "</td>".
      "<td>" . $each['from'] . "</td>".
      "<td>" . $each['year'] . "</td>".
      "</tr>";
  }
?>
    </table>
  </body>
</html>
