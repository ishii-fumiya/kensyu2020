<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>社員情報詳細</title>
  </head>

  <style type="text/css">
  #detail_table{
    width: 650px;
    margin: 0 auto;
  }

  #detail_form{
    width: 650px;
    margin: 0 auto;
  }

  #header{
    width: 100%;
    border-top-width: 0px;
    border-right-width: 0px;
    border-bottom-width: 0px;
    border-left-width: 0px;
  }

  #title{
    font-size: 200%;
  }

  #link{
    text-align: right;
  }

  #botan{
    text-align: right;
    margin-right: 218px;
  }
  </style>

  <body>
    <?php
      include("./include/header.php");
    ?>

    <script type="text/javascript">
    function goDel(){
      var result = window.confirm('削除を行います。よろしいですか？');
      if(result){
      // if(window.confirm('削除を行います。よろしいでしょうか？')){ //確認ダイアログ
        document.delete.submit();
      }
    }
    </script>

    <!-- DOM操作(Document Object Model)

  -->

    <?php
      include("./include/statics.php"); // 出身地を呼び出す
      include("./include/function.php"); // エラー処理を呼び出す
      $param_ID = "";
      if(isset($_GET['id']) && $_GET['id'] !="" && is_numeric($_GET['id'])){
        $param_ID = $_GET['id']; //データ内にidがあるかないのチェック
      }else{
        commonError(); //エラーの場合処理
      }

      $pdo = initDB();

      // $DB_DSN = "mysql:host=localhost; dbname=fishii; charset=utf8";
      // $DB_USER = "webaccess";
      // $DB_PW = "toMeu4rH";
      // $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

      if(isset($_GET['id']) && $_GET['id'] !=""){
      $query_str = "SELECT
                      member.member_ID,
                      member.name,
                      member.pref,
                      member.seibetu,
                      member.age,
                      section1_master.section_name,
                      grade_master.grade_name
                    FROM member
                    LEFT JOIN section1_master ON section1_master.ID = member.section_ID
                    LEFT JOIN grade_master ON grade_master.ID = member.grade_ID
                    WHERE member.member_ID = " .$param_ID;

      // echo $query_str;

      $sql = $pdo->prepare($query_str);
      $sql->execute();
      $result = $sql->fetchAll();
      if(count($result) != 1){
        commonError();
      }
    ?>

    <table border="1" style="border-collapse: collapse" id='detail_table'>
      <tr>
        <th>社員ID</th>
        <td>
          <?php
            echo $result[0]['member_ID'];
          ?>
        </td>
      </tr>

      <tr>
        <th>名前</th>
        <td>
          <?php
            echo $result[0]['name'];
          ?>
        </td>
      </tr>

      <tr>
        <th>出身地</th>
        <td>
          <?php
            echo $pref_array[$result[0]['pref']];
          ?>
        </td>
      </tr>

      <tr>
        <th>性別</th>
        <td>
          <?php
            echo $gender_array[$result[0]['seibetu']];
          ?>
        </td>
      </tr>

      <tr>
        <th>年齢</th>
        <td>
          <?php
            echo $result[0]['age'];
          ?>
        </td>
      </tr>

      <tr>
        <th>所属部署</th>
        <td>
          <?php
            echo $result[0]['section_name'];
          ?>
        </td>
      </tr>

      <tr>
        <th>役職</th>
        <td>
          <?php
            echo $result[0]['grade_name'];
          ?>
        </td>
      </tr>
    </table>

    <div id="botan">
      <form action='./entry_update01.php' method="GET" id='botan'>
        <input name="id" type="hidden" value = '<?php echo $result[0]['member_ID'];?>' >
        <input type="submit" value="編集">
      </form>

      <form action='./delete01.php' method="POST" name='delete' id='botan'>
        <input name="id" type="hidden" value = '<?php echo $result[0]['member_ID'];?>' >
        <input type="button" value="削除" onclick="goDel();">
      </form>
    </div>

    <?php
      }else{
        echo "社員IDが存在しません";
      }
    ?>

  </body>
</html>
