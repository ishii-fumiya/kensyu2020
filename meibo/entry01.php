<!DOCTYPE html>
<?php
  include("./include/statics.php");
  include("./include/function.php");
?>

<?php
    // $query_str = "SELECT *
    //               FROM section1_master";
    //
    // $sql = $pdo->prepare($query_str);
    // $sql->execute();
    // $result_section = $sql->fetchAll();
    $result_section = commonSection();

    // $query_str2 = "SELECT *
    //               FROM grade_master";
    //
    // $sql2 = $pdo->prepare($query_str2);
    // $sql2->execute();
    // $result_grade = $sql2->fetchAll();
    $result_grade = commonGrade();
?>

<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>新規社員情報登録</title>
    <script src="include/function.js"></script>
    <style type="text/css">
    #botan{
      text-align: right;
      margin-right: 135px;
    }

    #entryform{
      width: 650px;
      margin: 0 auto;
    }

    #header{
      width: 100%;
      border-top-width: 0px;
      border-right-width: 0px;
      border-bottom-width: 0px;
      border-left-width: 0px;
    }

    #title{
      font-size: 200%;
    }

    #link{
      text-align: right;
    }
    </style>
  </head>

  <body>
    <?php
      include("./include/header.php");
    ?>
      <form method='POST' action='./entry02.php' name='entry' id='entryform'>
        <table border="1" style="border-collapse: collapse">
          <tr>
            <p>
              <th>名前</th>
              <td><input type="text" name="namae"></td>
            </p>
          </tr>

          <tr>
            <th>出身地</th>
            <td>
              <select name="pref">
                <option value="">都道府県</option>
                <?php
                  foreach ($pref_array as $key => $value ){
                    echo "<option value='" . $key . "'>" . $value . "</option>";
                  }
                ?>
              </select>
            </td>
          </tr>

          <tr>
            <th>性別</th>
            <td>
              <input type="radio" name="seibetu" value="0" checked>男性
              <input type="radio" name="seibetu" value="1">女性
            </td>
          </tr>

          <tr>
            <th>年齢</th>
            <td><input type="munber" name="age" value="">才</td>
          </tr>

          <tr>
            <th><option value="ection">所属部署</option></th>
            <td>
              <?php
                foreach($result_section as $each){
                  if($each['ID'] == 1){
                    echo "<label><input type='radio' name='section' value='" . $each['ID'] . "'checked >" . $each['section_name'] . "</label>";
                  }else{
                    echo "<label><input type='radio' name='section' value='" . $each['ID'] . "'>" . $each['section_name'] . "</ladel>";
                  }
                }
              ?>
            </td>
            <!-- <td><input type="radio" name="new_section" value="1" checked>第一事業部
                <input type="radio" name="new_section" value="2">第二事業部
                <input type="radio" name="new_section" value="3">営業
                <input type="radio" name="new_section" value="4">総務
                <input type="radio" name="new_section" value="5">人事
            </td> -->
          </tr>

          <tr>
            <th><option value="grade">役職</option></th>
            <td>
              <?php
                foreach($result_grade as $each){
                  if($each['ID'] == 1){
                    echo "<label><input type='radio' name='grade' value='" . $each['ID'] . "'checked >" . $each['grade_name'] . "</label>";
                  }else{
                    echo "<label><input type='radio' name='grade' value='" . $each['ID'] . "'>" . $each['grade_name'] . "</ladel>";
                  }
                }
              ?>
            </td>
            <!-- <td><input type="radio" name="new_grade" value="1" checked>事業部長
                <input type="radio" name="new_grade" value="2">部長
                <input type="radio" name="new_grade" value="3">チームリーダー
                <input type="radio" name="new_grade" value="4">リーダー
                <input type="radio" name="new_grade" value="5">メンバー -->
          </tr>
        </table>

        <div id="botan">
          <input type="button" value="登録" onclick="goNew();">
          <input type="reset" value="リセット">
        </div>
      </form>

  </body>
</html>
