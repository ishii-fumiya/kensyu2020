<!DOCTYPE html>
<?php
  include("./include/function.php");

  // 入力値のパラメータチェック
  $param_namae = "";
  if(isset($_GET['user_name']) && $_GET['user_name'] !=""){
    $param_namae = $_GET['user_name'];
  }

  $param_sex = "";
  if(isset($_GET['sex']) && $_GET['sex'] !=""){
    $param_sex = $_GET['sex'];
  }

  $param_section = "";
  if(isset($_GET['section']) && $_GET['section'] !=""){
    $param_section = $_GET['section'];
  }

  $param_grade = "";
  if(isset($_GET['grade']) && $_GET['grade'] !=""){
    $param_grade = $_GET['grade'];
  }
    // パラメータチェック

  $pdo = initDB();

  // データベース接続
  // $DB_DSN = "mysql:host=localhost; dbname=fishii; charset=utf8";
  // $DB_USER = "webaccess";
  // $DB_PW = "toMeu4rH";
  // $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  // // データベース接続

  //実行する生SQL文
  $query_str = "SELECT
                  member.member_ID,
                  member.name,
                  member.seibetu,
                  section1_master.section_name,
                  grade_master.grade_name
                FROM member
                LEFT JOIN section1_master ON section1_master.ID = member.section_ID
                LEFT JOIN grade_master ON grade_master.ID = member.grade_ID
                WHERE 1=1"; //(WHERE 1=1)は全件表示
  //実行する生SQL文

  // 入力フォームに入力した値を引っ張ってくる
  if($param_namae != ""){
    $query_str .= " AND member.name LIKE '%" . $param_namae . "%'";
  }
  if(isset($_GET['sex']) && $_GET['sex'] != ""){
    $query_str .= " AND member.seibetu = " . $param_sex;
  }
  if(isset($_GET['section']) && $_GET['section'] != ""){
    $query_str .= " AND member.section_ID = " . $param_section;
  }
  if(isset($_GET['grade']) && $_GET['grade'] != ""){
    $query_str .= " AND member.grade_ID = " . $param_grade;
  }
  // 入力フォームに入力した値を引っ張ってくる

  // どの値をもってきているかを表示
  // echo $query_str;

  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
  // どの値をもってきているかを表示
?>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員一覧画面</title>

    <!-- bootstrap用 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <style type="text/css">
    #resulttable{
      width: 650px;
      margin: 0 auto;
    }

    #resultform{
      text-align : center;
      width: 650px;
      margin: 0 auto;
    }

    #botan{
      text-align: center;
      margin-top: 8px;
    }

    #header{
      width: 100%;
      border-top-width: 0px;
      border-right-width: 0px;
      border-bottom-width: 0px;
      border-left-width: 0px;
    }

    #title{
      font-size: 200%;
    }

    #link{
      text-align: right;
    }

    </style>

    <script type="text/javascript">
      function goClear(){
        document.search.user_name.value = "";
        document.search.sex.value = "";
        document.search.section.value = "";
        document.search.grade.value = "";
      }
    </script>
  </head>

  <body>
    <?php
      include("./include/header.php");
    ?>

    <!-- 入力部分 -->
    <form method='GET' action='./index.php' name='search' id='resultform'>
      <lable for="user_name">名前：</lable>
      <input type="text" name="user_name" value="<?php echo $param_namae; ?>"><br>

      <lable for="sex">性別：</lable>
      <select name="sex">
        <option value="" <?php if ($param_sex == "") { echo "selected" ; } ?>>すべて</option>
        <option value="0" <?php if ($param_sex == "0") { echo "selected" ; } ?>>女</option>
        <option value="1" <?php if ($param_sex == "1") { echo "selected" ; } ?>>男</option>
      </select>

      <lable for="section">部署：</lable>
      <select name='section'>
        <option value="">すべて</option>
        <?php
          $result_section = commonSection();
          foreach($result_section as $each){
            if($each['ID'] == $param_section){
              echo "<option value='" . $each['ID'] . "' selected>" . $each['section_name'] . "</option>";
            }else{
              echo "<option value='" . $each['ID'] . "'>" . $each['section_name'] . "</option>";
            }
          }
        ?>
      </select>

      <lable for="grade">役職：</lable>
      <select name='grade'>
        <option value="">すべて</option>
        <?php
          $result_grade = commonGrade();
          foreach($result_grade as $each){
            if($each['ID'] == $param_grade){
              echo "<option value='" . $each['ID'] . "' selected>" . $each['grade_name'] . "</option>";
            }else{
              echo "<option value='" . $each['ID'] . "'>" . $each['grade_name'] . "</option>";
            }
          }
        ?>
      </select><br>

      <div id="botan">
        <br><input type="submit" value="検索" class='btn btn-outline-primary'>
        <input type="button" value="リセット" onclick="goClear();" class='btn btn-outline-danger'>
      </div>
    </form>
    <!-- 入力部分 -->

    <hr/>
      <form style="width: 650px; margin: 0 auto; ">検索結果：
        <?php
          $result_number = count($result);
          echo $result_number;
        ?>
      </form>

    <!-- <table  border='1' style="border-collapse:collapse;"> -->
     <table class='table table-striped' id='resulttable'>
      <tr>
        <th>社員ID</th>
        <th>名前</th>
        <th>部署</th>
        <th>役職</th>
      </tr>

      <?php
        foreach($result as $each){
          echo
          "<tr>" .
            "<td>" . $each['member_ID'] . "</td>".
            "<td><a href='./detail01.php?id=". $each['member_ID'] ."'>" . $each['name'] . "</a></td>" .
            "<td>" . $each['section_name'] . "</td>".
            "<td>" . $each['grade_name'] . "</td>".
          "</tr>";
        }
      ?>
     </table>

  </body>
</html>
