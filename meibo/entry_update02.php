<!DOCTYPE html>
<?php
  include("./include/statics.php");
  include("./include/function.php");

  // パラメータチェック
  $param_id = "";
  if(isset($_POST['member_ID']) && $_POST['member_ID'] !=""){
    $param_id = $_POST['member_ID'];
  }else{
    commonError();
  }

  $param_name = "";
  if(isset($_POST['namae']) && $_POST['namae'] !=""){
    $param_name = $_POST['namae'];
  }else{
    commonError();
  }

  $param_pref = "";
  if(isset($_POST['pref']) && $_POST['pref'] !=""){
    $param_pref = $_POST['pref'];
  }else{
    commonError();
  }

  $param_sex = "";
  if(isset($_POST['seibetu']) && $_POST['seibetu'] !=""){
    $param_sex = $_POST['seibetu'];
  }else{
    commonError();
  }

  $param_age = "";
  if(isset($_POST['age']) && $_POST['age'] !="" && is_numeric($_POST['age'])){
    $param_age = $_POST['age'];
  }else{
    commonError();
  }

  $param_section = "";
  if(isset($_POST['section']) && $_POST['section'] !=""){
    $param_section = $_POST['section'];
  }else{
    commonError();
  }

  $param_grade = "";
  if(isset($_POST['grade']) && $_POST['grade'] !=""){
    $param_grade = $_POST['grade'];
  }else{
    commonError();
  }

  $pdo = initDB(); //データベース接続

  // $DB_DSN = "mysql:host=localhost; dbname=fishii; charset=utf8";
  // $DB_USER = "webaccess";
  // $DB_PW = "toMeu4rH";
  // $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

  $query_str = "UPDATE member SET
                      member.name ='" . $_POST['namae'] . "',
                      member.pref = '" . $_POST['pref'] . "',
                      member.seibetu = '" . $_POST['seibetu'] . "',
                      member.age = '" . $_POST['age'] . "',
                      member.section_ID = '" . $_POST['section'] . "',
                      member.grade_ID = '" . $_POST['grade'] . "'
                WHERE member.member_ID = " . $_POST['member_ID'];

  try{
    $sql = $pdo->prepare($query_str);
    $sql->execute();
  }catch(PDOException $e){
    print $e->getMessage();
  }
  // echo $query_str;

  $url = "detail01.php?id=" . $param_id;
       header('Location: ' . $url);
  exit;
?>
