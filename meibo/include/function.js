// 名前からスペースの除去
function goNew(){
  var temp_name = document.entry.namae.value;
  temp_name = temp_name.replace(/\s+/g,"");
  document.entry.namae.value = temp_name;
  if(temp_name == ""){
    alert('名前は必須です');
    return false;
  }

  if(document.entry.pref.value == ""){
    window.alert('都道府県は必須です');
    return false;
  }

  // 年齢上限チェック
  // if(document.entry.new_age.value == "" || document.entry.new_age.value >= 100 || document.entry.new_age.value <= 0){
  var temp_age = document.entry.age.value;
  var regex = new RegExp(/^[0-9]+$/);
  if(temp_age == "" || temp_age < 1 || temp_age>99 || !regex.test(temp_age)){
    window.alert('年齢は必須です/数値を入力してください/1-99の範囲で入力してください');
    return false;
  }

  if(window.confirm('更新を行います。よろしいですか？')){
    document.entry.submit();
  }
}
