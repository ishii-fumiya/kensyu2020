<?php
   // エラー処理
  function commonError(){
    echo "エラーが発生しました。<br/>";
    echo "<a href='./index.php'>トップページに戻る</a>";
    echo "</body>";
    echo "</html>";
    exit();
  }

  //データベース接続
  function initDB(){
    $DB_DSN = "mysql:host=localhost; dbname=fishii; charset=utf8";
    $DB_USER = "webaccess";
    $DB_PW = "toMeu4rH";
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
    return $pdo;
  }

  // 部署データ
  function commonSection(){
    $pdo = initDB();
    $query_str = "SELECT *
                  FROM section1_master";

    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result_section = $sql->fetchAll();
    return $result_section;
  }
  
  // 役職データ
  function commonGrade(){
    $pdo = initDB();
    $query_str2 = "SELECT *
                   FROM grade_master";

    $sql2 = $pdo->prepare($query_str2);
    $sql2->execute();
    $result_grade = $sql2->fetchAll();
    return $result_grade;
  }
?>
