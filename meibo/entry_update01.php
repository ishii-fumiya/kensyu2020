<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>既存社員情報修正</title>
    <script src="include/function.js"></script>
  </head>

  <?php
    include("./include/statics.php");
    include("./include/function.php");
    $param_ID = "";
    if(isset($_GET['id']) && $_GET['id'] !="" && is_numeric($_GET['id'])){
      $param_ID = $_GET['id']; //データ内にidがあるかないのチェック
    }else{
      commonError();
    }
    $pdo = initDB();

    // $DB_DSN = "mysql:host=localhost; dbname=fishii; charset=utf8";
    // $DB_USER = "webaccess";
    // $DB_PW = "toMeu4rH";
    // $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

    $query_str = "SELECT
                    member.member_ID,
                    member.name,
                    member.pref,
                    member.seibetu,
                    member.age,
                    member.section_ID,
                    member.grade_ID
                  FROM member
                  WHERE member.member_ID = " .$param_ID;

    // echo $query_str;

    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchAll();
    if(count($result) != 1){
      commonError();
    }
  ?>

  <body>
    <?php
      include("./include/header.php");
    ?>

    <form method='POST' action='./entry_update02.php' name='entry'>
      <table border="1" style="border-collapse: collapse">
        <tr>
          <th>社員ID</th>
          <td>
            <?php
              echo $result[0]['member_ID'];
            ?>
          </td>
        </tr>

        <tr>
          <th>名前</th>
          <td><input name="namae" type="text" value = <?php echo $result[0]['name']; ?>></td>
        </tr>

        <tr>
          <th>出身地</th>
          <td>
            <select name="pref">
              <?php
                foreach($pref_array as $key => $value){
                  if($result[0]['pref'] == $key){
                    echo "<option value='" . $key . "' selected>" . $value . "</option>";
                  }else{
                    echo "<option value='" . $key . "' >" . $value . "</option>";
                  }
                }
              ?>
            </select>
          </td>
        </tr>

        <tr>
          <th>性別</th>
          <td>
            <input type="radio" name="seibetu" value="0" <?php if($result[0]['seibetu'] == "0") {echo 'checked';} ?>>男
            <input type="radio" name="seibetu" value="1" <?php if($result[0]['seibetu'] == "1") {echo 'checked';} ?>>女
          </td>
        </tr>

        <tr>
          <th>年齢</th>
          <td><input type="munber" name="age" value=<?php echo $result[0]['age']; ?>>才</td>
        </tr>

        <tr>
          <th>所属部署</th>
          <td>
            <?php
              $result_section = commonSection();
              foreach($result_section as $each){
                if($each['ID'] == $result[0]['section_ID']){
                  echo "<label><input type='radio' name='section' value='" . $each['ID'] . "'checked >" . $each['section_name'] . "</label>";
                }else{
                  echo "<label><input type='radio' name='section' value='" . $each['ID'] . "'>" . $each['section_name'] . "</ladel>";
                }
              }
            ?>
          </td>
        </tr>

        <tr>
          <th>役職</th>
          <td>
            <?php
              $result_grade = commonGrade();
              foreach($result_grade as $each){
                if($each['ID'] == $result[0]['grade_ID']){
                  echo "<label><input type='radio' name='grade' value='" . $each['ID'] . "'checked >" . $each['grade_name'] . "</label>";
                }else{
                  echo "<label><input type='radio' name='grade' value='" . $each['ID'] . "'>" . $each['grade_name'] . "</ladel>";
                }
              }
            ?>
          </td>
        </tr>
      </table>

      <input name="member_ID" type="hidden" value = '<?php echo $result[0]['member_ID'];?>' >
      <input type="button" value="登録" onclick="goNew()">
      <input type="reset" value="リセット">
    </form>

  </body>
</html>
