<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>計算ページ</title>
  </head>

  <body>
    <form method="POST" action="tax.php">
      <table border="1">
        <tr>
        　<th>商品名</th>
        　<th>価格（単価：円、税抜き）</th>
        　<th>個数</th>
        　<th>税率</th>
        </tr>
        <tr>
        　<td><input type="text" name="shouhinna1"></td>
        　<td><input type="text" name="kakaku1"></td>
        　<td><input type="text" name="kosu1">個</td>
        　<td><input type="radio" name="tax1" value="8" checked>8%<input type="radio" name="tax1" value="10">10%</td>
        </tr>
        <tr>
          <td><input type="text" name="shouhinna2"></td>
          <td><input type="text" name="kakaku2"></td>
          <td><input type="text" name="kosu2">個</td>
          <td><input type="radio" name="tax2" value="8" checked>8%<input type="radio" name="tax2" value="10">10%</td>
        </tr>
        <tr>
          <td><input type="text" name="shouhinna3"></td>
          <td><input type="text" name="kakaku3"></td>
          <td><input type="text" name="kosu3">個</td>
          <td><input type="radio" name="tax3" value="8" checked>8%<input type="radio" name="tax3" value="10">10%</td>
        </tr>
        <tr>
          <td><input type="text" name="shouhinna4"></td>
          <td><input type="text" name="kakaku4"></td>
          <td><input type="text" name="kosu4">個</td>
          <td><input type="radio" name="tax4" value="8" checked>8%<input type="radio" name="tax4" value="10">10%</td>
        </tr>
        <tr>
          <td><input type="text" name="shouhinna5"></td>
          <td><input type="text" name="kakaku5"></td>
          <td><input type="text" name="kosu5">個</td>
          <td><input type="radio" name="tax5" value="8" checked>8%<input type="radio" name="tax5" value="10">10%</td>
        </tr>
      </table>

      <input type="submit" value="送信">
      <input type="reset" value="リセット">
    </form>

   <p>ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー</p>
   <!---出力--->
   <table border="1">
    <tr>
      <th>商品名</th>
      <th>価格（単価：円、税抜き）</th>
      <th>個数</th>
      <th>税率</th>
      <th>小計</th>
    </tr>
    <tr>
      <td><?php echo $_POST['shouhinna1']; ?></td>
      <td><?php echo $_POST['kakaku1']; ?></td>
      <td><?php echo $_POST['kosu1']; ?></td>
      <td><?php echo $_POST['tax1']; ?></td>
      <td>
        <?php
        $price1 = $_POST['tax1'] / 100 + 1;
        $subtotal1 = $_POST['kakaku1'] * $_POST['kosu1'] * $price1;
        echo $subtotal1;
        ?>
      </td>
    </tr>
    <tr>
      <td><?php echo $_POST['shouhinna2']; ?></td>
      <td><?php echo $_POST['kakaku2']; ?></td>
      <td><?php echo $_POST['kosu2']; ?></td>
      <td><?php echo $_POST['tax2']; ?></td>
      <td>
        <?php
        $price2 = $_POST['tax2'] / 100 + 1;
        $subtotal2 = $_POST['kakaku2'] * $_POST['kosu2'] * $price2;
        echo $subtotal2;
        ?>
      </td>
    </tr>
    <tr>
      <td><?php echo $_POST['shouhinna3']; ?></td>
      <td><?php echo $_POST['kakaku3']; ?></td>
      <td><?php echo $_POST['kosu3']; ?></td>
      <td><?php echo $_POST['tax3']; ?></td>
      <td>
        <?php
        $price3 = $_POST['tax3'] / 100 + 1;
        $subtotal3 = $_POST['kakaku3'] * $_POST['kosu3'] * $price3;
        echo $subtotal3;
        ?>
      </td>
    </tr>
    <tr>
      <td><?php echo $_POST['shouhinna4']; ?></td>
      <td><?php echo $_POST['kakaku4']; ?></td>
      <td><?php echo $_POST['kosu4']; ?></td>
      <td><?php echo $_POST['tax4']; ?></td>
      <td>
        <?php
        $price4 = $_POST['tax4'] / 100 + 1;
        $subtotal4 = $_POST['kakaku4'] * $_POST['kosu4'] * $price4;
        echo $subtotal4;
        ?>
      </td>
    </tr>
    <tr>
      <td><?php echo $_POST['shouhinna5']; ?></td>
      <td><?php echo $_POST['kakaku5']; ?></td>
      <td><?php echo $_POST['kosu5']; ?></td>
      <td><?php echo $_POST['tax5']; ?></td>
      <td>
        <?php
        $price5 = $_POST['tax5'] / 100 + 1;
        $subtotal5 = $_POST['kakaku5'] * $_POST['kosu5'] * $price5;
        echo $subtotal5;
        ?>
      </td>
    </tr>
    <tr>
      <td colspan="4">合計</td>
      <td>
      <?php
      $total = $subtotal1 + $subtotal2 + $subtotal3 + $subtotal4 + $subtotal5;
      echo $total;
      ?>
      </td>
    </tr>
   </table>
  </body>
</html>
