<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>ループ処理</title>
  </head>
  <body>
    <form method='POST' action='./loop03.php'>
      <input type="text" name="gyou">行×
      <input type="text" name="retu">列<br>
      <input type="submit" value="送信">
      <input type="reset" value="リセット">
    </form>

       <table border="1">
        <?php
          for($i=1; $i <= $_POST['gyou']; $i++){
            echo "<tr>";
             for($j=1; $j <= $_POST['retu']; $j++){
               echo "<td>$i-$j</td>";
             }
            echo "</tr>";
          }
        ?>
       </table>

  </body>
</html>
