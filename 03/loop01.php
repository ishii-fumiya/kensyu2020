<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>ループ処理</title>
  </head>
  <body>
    <form method='POST' action='./loop01.php'>
      <input type="text" name="line">行のテーブルを生成する<br>
      <input type="submit" value="送信">
      <input type="reset" value="リセット">

    </form>
    <table border="1" style="border-collapse:collapse;">
      <?php
        for($i=0; $i < $_POST['line']; $i++){
          echo "<tr><td>石井</td><td>ふみや</td><td>です</td></tr>";
        }
      ?>
    </table>
  </body>
</html>
