<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列</title>
  </head>

  <body>
  <?php
    // サンプル
    $fruit = array("りんご", "すいか", "みかん", "なし", "イチゴ", "かき","ぶどう","れもん");

    echo $fruit[3]; // なし と表示される

    echo $fruit[0]; // りんご と表示される

    echo $fruit[9]; // エラーになる

    $fruit[2] = "いちじく"; // 上書きされる

    $fruit[6] = "キウイ"; // 追加される

    var_dump($fruit); // 配列の中身がすべて表示される

    echo "<table border=\"1\">";
    for($i=0; $i < count($fruit) ; $i++){
      echo "<tr><td>" . $fruit[$i] . "</td></tr>";
    }
    echo "</table>";

      echo "<hr>";
      foreach($fruit as $key => $value){  
        echo $key . "番目の要素は" . $value . "です。<br/>";
      }
      echo "<hr>";

      // 検索する文字列
      $needle = "すいか";

      if(in_array($needle, $fruit)) {
        echo $needle . " がfruitの要素の値に存在しています";
      } else {
        echo $needle . " がfruitの要素の値に存在しません";
       }
  ?>

  配列の個数は<?php echo count($fruit); ?>
<!-- ブラウザで表示させるときは<pre>で囲むと見やすく表示される -->
    <pre>
      <?php var_dump($fruit);?>

    </pre>
  </body>

</thml>
