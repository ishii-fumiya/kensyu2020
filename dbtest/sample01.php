<!DOCTYPE html>
<?php
  //common
  //include("./include/functions.php");
  $DB_DSN = "mysql:host=localhost; dbname=fishii; charset=utf8";
  $DB_USER = "webaccess";
  $DB_PW = "toMeu4rH";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  //$sql = initDB();

  //実行する生SQL文
  $query_str = "SELECT *
                FROM test_table01";

  //$query_str = "SELECT" * FROM 'test_teble' WHERE dish_name LIKE '%の%' AND genre = 'おつまみ'";

  echo $query_str; //実行したSQLを画面に表示
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>データベース</title>
  </head>

  <body>
    <pre>
      <?php
      var_dump($result);
      ?>
    </pre>

      <?php
      foreach($result as $each){

        //var_dump($each);
        echo $each['dish_name'] . " : " . $each['price'] . "円";
        echo "<hr/>";
      }
      ?>

      <table border='1'>
      <tr>
        <th>ID</th>
        <th>メニュー名</th>
        <th>種類</th>
        <th>値段</th>
        <th>メモ</th>
      </tr>

      <?php
        foreach($result as $each){
          echo
          "<tr>" .
            "<td>" . $each['id'] . "</td>".
            "<td>" . $each['dish_name'] . "</td>".
            "<td>" . $each['genre'] . "</td>".
            "<td>" . $each['price'] . "</td>".
            "<td>" . $each['memo'] . "</td>".
          "</tr>";
        }
      ?>
    </table>
  </body>
</html>
